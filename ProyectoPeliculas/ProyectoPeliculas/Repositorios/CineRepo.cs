﻿using System;
using System.Linq;
using System.Collections.Generic;
using ProyectoPeliculas.Data;
using ProyectoPeliculas.Interfaces;
using ProyectoPeliculas.Models;

namespace ProyectoPeliculas.Repositorios
{
    public class CineRepo : ICine
    {
        private readonly ApplicationDbContext _context;

        public CineRepo(ApplicationDbContext peliculaConext)
        {
            _context = peliculaConext;
        }

        public Cine Get(int cineId) => _context.Cines.FirstOrDefault(c => c.Id == cineId);
        public void Delete(Cine cine) => _context.Cines.Remove(cine);
        public List<Cine> GetAll() => _context.Cines.ToList();
        public Cine GetById(int cineId) => _context.Cines.FirstOrDefault(c => c.Id == cineId);
        public void Insert(Cine cine) => _context.Cines.Add(cine);
        public void Update(Cine cine) => _context.Cines.Update(cine);


    }
}
