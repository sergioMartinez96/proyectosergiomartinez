﻿using System;
using ProyectoPeliculas.Data;
using ProyectoPeliculas.Interfaces;

namespace ProyectoPeliculas.Repositorios
{
    public class UnitOfWorkRepo : IUnitOfWorks
    {
        private readonly ApplicationDbContext _context;
        private IPelicula _PeliculaRepo;
        private ICategoria _CategoriaRepo;
        private ICine _CineRepo;

        public UnitOfWorkRepo(ApplicationDbContext peliculaConext)
        {
            _context = peliculaConext;
        }

        public IPelicula Pelicula => _PeliculaRepo = _PeliculaRepo ?? new PeliculaRepo(_context);

        public ICategoria Categoria => _CategoriaRepo = _CategoriaRepo ?? new CategoriaRepo(_context);
        public ICine Cine => _CineRepo = _CineRepo ?? new CineRepo(_context);


        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
