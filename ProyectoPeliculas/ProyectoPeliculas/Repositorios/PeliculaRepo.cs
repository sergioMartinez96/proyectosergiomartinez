﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ProyectoPeliculas.Data;
using ProyectoPeliculas.Interfaces;
using ProyectoPeliculas.Models;

namespace ProyectoPeliculas.Repositorios
{
    public class PeliculaRepo : IPelicula
    {
        private readonly ApplicationDbContext _context;

        public PeliculaRepo(ApplicationDbContext conext)
        {
            _context = conext;
        }

        public Pelicula GetByMovieId(int movieId)
        {
            return _context.Peliculas
                .Include(p => p.Comentarios)
                .ThenInclude(c => c.Pelicula)
                .Include(p => p.PeliculaCategorias)
                .ThenInclude(pc => pc.Pelicula)
                .Include(p => p.PeliculasFavoritas)
                .ThenInclude(pf => pf.Pelicula)
                .FirstOrDefault(p => p.MovieId == movieId);
        }

        public Pelicula GetByPelicuaId(int peliuclaId) => _context.Peliculas.FirstOrDefault(p => p.PeliculaId == peliuclaId);

        public List<Pelicula> GetAll() => _context.Peliculas.ToList();

        public void Insert(Pelicula pelicula) => _context.Peliculas.Add(pelicula);

        public void Update(Pelicula pelicula) => _context.Peliculas.Update(pelicula);

        public void Delete(Pelicula pelicula) => _context.Peliculas.Remove(pelicula);
    }
}
