﻿using System;
using System.Linq;
using System.Collections.Generic;
using ProyectoPeliculas.Data;
using ProyectoPeliculas.Interfaces;
using ProyectoPeliculas.Models;

namespace ProyectoPeliculas.Repositorios
{
    public class CategoriaRepo : ICategoria
    {
        private readonly ApplicationDbContext _context;

        public CategoriaRepo(ApplicationDbContext peliculaConext)
        {
            _context = peliculaConext;
        }

        public Categoria Get(int genreId) => _context.Categorias.FirstOrDefault(c => c.GenreId == genreId);

        public Categoria GetById(int categoriaId) => _context.Categorias.FirstOrDefault(c => c.CategoriaId == categoriaId);

        public List<Categoria> GetAll() => _context.Categorias.ToList();

        public void Insert(Categoria categoria) => _context.Categorias.Add(categoria);

        public void Update(Categoria categoria) => _context.Categorias.Update(categoria);

        public void Delete(Categoria categoria) => _context.Categorias.Remove(categoria);
    }
}
