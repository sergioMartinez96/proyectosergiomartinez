﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using ProyectoPeliculas.Models;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ProyectoPeliculas.Data
{
    public class ApplicationDbContext: IdentityDbContext<ApplicationUser, IdentityRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Pelicula> Peliculas { get; set; }
        public DbSet<Cine> Cines { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PeliculaCategoria>()
                .HasKey(c => new { c.PeliculaId, c.CategoriaId });

            modelBuilder.Entity<PeliculaCategoria>()
                .HasOne(pc => pc.Pelicula)
                .WithMany(p => p.PeliculaCategorias)
                .HasForeignKey(pc => pc.PeliculaId);

            modelBuilder.Entity<PeliculaCategoria>()
                .HasOne(pc => pc.Categoria)
                .WithMany(c => c.PeliculaCategorias)
                .HasForeignKey(pc => pc.CategoriaId);

            modelBuilder.Entity<Comentario>()
              .HasOne(pc => pc.Pelicula)
              .WithMany(c => c.Comentarios);

            modelBuilder.Entity<ApplicationUser>()
                .HasOne(app => app.Usuario)
                .WithMany(u => u.ApplicationUsers);

            modelBuilder.Entity<PeliculaFavorita>()
                .HasKey(c => new { c.UserId, c.PeliculaId });

            modelBuilder.Entity<PeliculaFavorita>()
                .HasOne(pc => pc.ApplicationUser)
                .WithMany(c => c.PeliculaFavoritas)
                .HasForeignKey(pc => pc.UserId);

            modelBuilder.Entity<PeliculaFavorita>()
               .HasOne(pc => pc.Pelicula)
               .WithMany(c =>c.PeliculasFavoritas)
               .HasForeignKey(pc => pc.PeliculaId
               );

            modelBuilder.Entity<IdentityRole>()
                .HasData(new IdentityRole[]
                {
                  new IdentityRole() { Name = "Administrador", NormalizedName = "administrador".ToUpper().Normalize() },
                  new IdentityRole() { Name = "Usuario", NormalizedName = "usuario".ToUpper().Normalize() }
                }
               );

            modelBuilder.Entity<Categoria>().HasData(InsertarCategorias());
            
        }
                
       public Categoria[] InsertarCategorias()
       {
            List<Categoria> ListaCategorias = new List<Categoria>();
            Categoria[] CategoriasJson = new Categoria[19];
            using (StreamReader reader = new StreamReader(@"Categorias.json"))
            {
                ListaCategorias = JsonConvert.DeserializeObject<List<Categoria>>(reader.ReadToEnd());
                int contador = 0;
                foreach (var item in ListaCategorias)
                {
                    CategoriasJson[contador] = item;
                    contador++;
                }
            }

            return CategoriasJson;
       }
    }
}