﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoPeliculas.ViewModel
{
    public class ListadoUsuarios
    {
        public string IdUsuario{ get; set; }
        public string Email { get; set; }
        public string Rol { get; set; }
    }
}
