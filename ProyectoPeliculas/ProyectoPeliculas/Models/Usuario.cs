﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoPeliculas.Models
{
    public class Usuario
    {
        [Required]
        public int UsuarioId { get; set; }

        [StringLength(100)]
        public string Nombre { get; set; }

        [StringLength(100)]
        public string Apellido { get; set; }

        [StringLength(150)]
        public string Pais { get; set; }

        public Byte[] Imagen{ get; set; }
 
        public List<ApplicationUser> ApplicationUsers { get; set; }
    }
}
