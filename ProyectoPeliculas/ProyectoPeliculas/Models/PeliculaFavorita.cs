﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ProyectoPeliculas.Models
{
    public class PeliculaFavorita
    {
        public int PeliculaId { get; set; }

        public Pelicula Pelicula { get; set; }

        [Required, StringLength(450)]
        public string UserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
    }
}
