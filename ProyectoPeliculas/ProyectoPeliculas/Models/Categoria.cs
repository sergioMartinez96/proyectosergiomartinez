﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProyectoPeliculas.Models
{
    public class Categoria
    {
        
        public int CategoriaId { get; set; }

        [Required]
        public int GenreId { get; set; }

        [Required, StringLength(45)]
        public string Name { get; set; }

        public List<PeliculaCategoria> PeliculaCategorias { get; set; } = new List<PeliculaCategoria>();
    }
}
