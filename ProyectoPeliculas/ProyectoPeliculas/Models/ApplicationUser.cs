﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace ProyectoPeliculas.Models
{
    public class ApplicationUser: IdentityUser
    {
        public Usuario Usuario { get; set; }
        public List<PeliculaFavorita> PeliculaFavoritas { get; set; } = new List<PeliculaFavorita>();
    }
}
