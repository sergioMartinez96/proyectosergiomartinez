﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoPeliculas.Models
{
    public class Pelicula
    {
     
        public int PeliculaId { get; set; }

        [Required]
        public int MovieId { get; set; }

        [Required, StringLength(100)]
        public string Titulo { get; set; }

        [Required, Column(TypeName = "nvarchar(max)")]
        public string ImageUrl { get; set; }

        [Required, StringLength(1000)]
        public string Descripcion { get; set; }

        [StringLength(5)]
        public string Languaje { get; set; }

        public double Votos { get; set; }

        public double TotalVotos { get; set; }

        public double PersonaVotos { get; set; }

        [Required]
        public int CineId { get; set; }

        public Cine Cine { get; set; }
        
        public List<Comentario> Comentarios { get; set; } = new List<Comentario>();

        public List<PeliculaCategoria> PeliculaCategorias { get; set; } = new List<PeliculaCategoria>();        

        public List<PeliculaFavorita> PeliculasFavoritas { get; set; } = new List<PeliculaFavorita>();
    }
}

