﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectoPeliculas.Models
{
    public class PeliculaCategoria
    {
        public int PeliculaId { get; set; }

        public Pelicula Pelicula { get; set; }

        public int CategoriaId { get; set; }

        public Categoria Categoria { get; set; }
    }
}
