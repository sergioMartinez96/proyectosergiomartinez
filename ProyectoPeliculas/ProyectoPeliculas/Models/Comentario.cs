﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoPeliculas.Models
{
    public class Comentario
    {
        public int ComentarioId { get; set; }

        public string Autor { get; set; }

        [Required, StringLength(200)]
        public string Descripcion { get; set; }

        [Required]
        public DateTime FechaPublicacion { get; set; }

        [Required]
        public int PeliculaId { get; set; }

        public Pelicula Pelicula { get; set; }
    }
}
