﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProyectoPeliculas.Interfaces;

namespace ProyectoPeliculas.Helpers
{
    // Implementacion a futuro

    public class Helper
    {
        private readonly IUnitOfWorks _unitOfWorkRepo;

        public Helper(IUnitOfWorks unitOfWorkRepo)
        {
            _unitOfWorkRepo = unitOfWorkRepo;
        }
    }
}
