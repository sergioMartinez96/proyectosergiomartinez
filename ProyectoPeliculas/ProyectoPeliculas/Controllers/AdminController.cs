﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ProyectoPeliculas.Interfaces;
using ProyectoPeliculas.Models;
using ProyectoPeliculas.ViewModel;

namespace ProyectoPeliculas.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IUnitOfWorks _unitOfWorkRepo;

        public AdminController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IUnitOfWorks unitOfWorkRepo)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _unitOfWorkRepo = unitOfWorkRepo;
        }

        [AllowAnonymous]
        public IActionResult AccesDenied() => View();

        public async Task<IActionResult> Index()
        {
            List<ListadoUsuarios> lista = new List<ListadoUsuarios>();
            var usuarios = _userManager.Users.ToList();
            var rolUsuario = await _userManager.GetUsersInRoleAsync("Usuario");

            foreach (var item in usuarios)
            {
                if (rolUsuario.Contains(item) && item.LockoutEnd==null )
                {
                    lista.Add(new ListadoUsuarios
                    {
                        IdUsuario = item.Id,
                        Email = item.Email,
                        Rol = "Usuario"
                    });
                }
            }

            return View(lista);
        }

        public async Task<IActionResult> Bloquear(string userId)
        {
            var user = _userManager.Users.FirstOrDefault(u => u.Id == userId);
            user.LockoutEnabled = true;
            DateTime today = DateTime.Now;
            DateTime answer = today.AddYears(5);
            user.LockoutEnd = answer;

            await _userManager.UpdateAsync(user);
            return Redirect("Index");
        }

        public IActionResult ListadoCategorias ()
        {
            var categoria = _unitOfWorkRepo.Categoria.GetAll();
            return View(categoria);
        }

        public IActionResult EliminarCategoria (int idCategoria)
        {
            var categoria = _unitOfWorkRepo.Categoria.GetById(idCategoria);
            _unitOfWorkRepo.Categoria.Delete(categoria);
            _unitOfWorkRepo.Save();

            return Redirect("ListadoCategorias");

        }

        public IActionResult EliminarComentario (int peliculaId, int comentarioId, string url)
        {
            var pelicula = _unitOfWorkRepo.Pelicula.GetByMovieId(peliculaId);
            var comentario = pelicula.Comentarios.FirstOrDefault(c => c.ComentarioId == comentarioId);

            if (comentario != null) { pelicula.Comentarios.Remove(comentario); }

            _unitOfWorkRepo.Pelicula.Update(pelicula);
            _unitOfWorkRepo.Save();

            return Redirect(url);
        }

        public IActionResult ListadoCines()
        {
            var cine = _unitOfWorkRepo.Cine.GetAll();
            return View(cine);
        }

        public IActionResult EliminarCine(int idCine)
        {
            var cine = _unitOfWorkRepo.Cine.GetById(idCine);
            _unitOfWorkRepo.Cine.Delete(cine);
            _unitOfWorkRepo.Save();

            return Redirect("ListadoCines");

        }

        [HttpGet]
        public IActionResult InsertarCine()
        {
            return View();
        }

        [HttpPost]
        public IActionResult InsertarCine(Cine cine)
        {
            if (ModelState.IsValid)
            {
                _unitOfWorkRepo.Cine.Insert(cine);
                _unitOfWorkRepo.Save();
                return Redirect("ListadoCines");
            }

            return View();
            
        }

        [HttpGet]
        public IActionResult EditarCine(int idCine)
        {
            Cine cine = _unitOfWorkRepo.Cine.GetById(idCine);
            //_unitOfWorkRepo.Cine.Update(idCine);
            return View(cine);
        }

        [HttpPost]
        public IActionResult EditarCine(Cine cine)
        {

            _unitOfWorkRepo.Cine.Update(cine);
            _unitOfWorkRepo.Save();
            return RedirectToAction(nameof(ListadoCines));
        }
    }
}
