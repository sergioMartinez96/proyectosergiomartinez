﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;
using ProyectoPeliculas.API;
using ProyectoPeliculas.Models;
using ProyectoPeliculas.Interfaces;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace ProyectoPeliculas.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitOfWorks _unitOfWorkRepo;
        

        public HomeController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, IUnitOfWorks unitOfWorkRepo)
        {
            _userManager = userManager;
            _unitOfWorkRepo = unitOfWorkRepo;
            _signInManager = signInManager;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index(int page = 1)
        {
            SearchContainer<SearchMovie> movies = await TheMovieDB.GetPopularMovies(page);
            List<Categoria> categorias = _unitOfWorkRepo.Categoria.GetAll();
            ViewData["categorias"] = categorias;
            return View("Index", movies);
        }

        [AllowAnonymous]
        public async Task<IActionResult> NowPlaying(int page = 1)
        {
            SearchContainer<SearchMovie> movies = await TheMovieDB.GetNowPlayingMovies(page);
            List<Categoria> categorias = _unitOfWorkRepo.Categoria.GetAll();
            ViewData["categorias"] = categorias;
            return View("Index", movies);
        }

        [AllowAnonymous]
        public async Task<IActionResult> UpComing(int page = 1)
        {
            SearchContainer<SearchMovie> movies = await TheMovieDB.GetUpComingMovies(page);
            List<Categoria> categorias = _unitOfWorkRepo.Categoria.GetAll();
            ViewData["categorias"] = categorias;
            return View("Index", movies);
        }

        [AllowAnonymous]
        public async Task<IActionResult> TopRate(int page = 1)
        {
            SearchContainer<SearchMovie> movies = await TheMovieDB.GetTopRateMovies(page);
            List<Categoria> categorias = _unitOfWorkRepo.Categoria.GetAll();
            ViewData["categorias"] = categorias;
            return View("Index", movies);
        }
    
        [AllowAnonymous]
        public async Task<IActionResult> Search(int page = 1)
        {
            SearchContainer<SearchMovie> searchMovie = await TheMovieDB.SearchMovie(page);
            ViewData["query"] = TheMovieDB.Query;
            return View("Search", searchMovie);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Search(string query)
        {
            TheMovieDB.Query = query;
            return Redirect("Search");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult SearchCine()
        {
            ViewData["mensaje"] = "";
            return View("ListadoCines", new List<Cine>());
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult SearchCine(string query)
        {

            List<Cine> cine = _unitOfWorkRepo.Cine.GetAll().FindAll(x => x.Ciudad == query);
            if (cine.Count() > 0)
            {
                ViewData["mensaje"] = "";
                return View("ListadoCines",cine);
            }

            ViewData["mensaje"] = "No existe ningun cine en la ciudad introducida";
            return View("ListadoCines", new List<Cine>());
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult verPeliculas(int id)
        {

            List<Pelicula> peliculas = _unitOfWorkRepo.Pelicula.GetAll().FindAll(x => x.CineId == id);
            if (peliculas.Count() > 0)
            {
                ViewData["mensaje"] = "";
                return View("VerPeliculas", peliculas);
            }

            ViewData["mensaje"] = "No existe ninguna pelicula en este cine";
            return View("VerPeliculas", new List<Pelicula>());
        }

       
        [AllowAnonymous]
        public async Task<IActionResult> GetGenre(int genreId = 0, int page = 1)
        {
            TheMovieDB.GenreId = (genreId >= 1) ? genreId : TheMovieDB.GenreId;
            SearchContainer<SearchMovie> searchMovies = await TheMovieDB.SearchByGenre(page);
            ViewData["query"] = string.Empty;
            ViewData["currentPage"] = TheMovieDB.CurrentPage;
            return View("Search", searchMovies);
        }

        [AllowAnonymous]
        public async Task<IActionResult> MovieDetail(int movieId)
        {
            Movie movie = await TheMovieDB.GetMovie(movieId);
            int countCine = _unitOfWorkRepo.Cine.GetAll().Count();
            Random rnd = new Random();


            string userId = (_signInManager.IsSignedIn(User)) ? _userManager.GetUserId(User) : "";

            var user = _userManager.Users
                .Include(app => app.PeliculaFavoritas)
                .ThenInclude(p => p.Pelicula)
                .FirstOrDefault(u => u.Id == userId);

            Pelicula pelicula = _unitOfWorkRepo.Pelicula.GetByMovieId(movie.Id);
            List<PeliculaFavorita> favoritas = (user != null) ? user.PeliculaFavoritas : new List<PeliculaFavorita>();

            if (pelicula == null)
            {
                List<PeliculaCategoria> peliculaCategoria = new List<PeliculaCategoria>();

                foreach (var item in movie.Genres)
                {
                    var genero = _unitOfWorkRepo.Categoria.Get(item.Id);
                    if (genero != null)
                    {
                        peliculaCategoria.Add(new PeliculaCategoria() {
                            CategoriaId = genero.CategoriaId
                        });
                    }
                }


                _unitOfWorkRepo.Pelicula.Insert(new Pelicula()
                {
                    MovieId = movie.Id,
                    Titulo = movie.Title,
                    ImageUrl = movie.PosterPath,
                    Descripcion = movie.Overview,
                    Languaje = movie.OriginalLanguage,
                    Votos = 0,
                    TotalVotos = 0,
                    PeliculaCategorias = peliculaCategoria,
                    CineId = rnd.Next(1, countCine+1)
                });
            }

            _unitOfWorkRepo.Save();
            ViewData["Voto"] = (pelicula == null) ? 0 : pelicula.Votos;
            ViewData["forecolor"] = (favoritas.FirstOrDefault(p => p.Pelicula.MovieId == movieId) != null) ? "text-danger" : "";
            ViewData["movieId"] = movieId;
            ViewData["comentarios"] = (pelicula !=null) ? pelicula.Comentarios : new List<Comentario>(); 

            return View("Movie", movie);
        }

        [HttpPost]
        public async Task<IActionResult> AddFavorite(string url, int movieId)
        {
            string userId = _userManager.GetUserId(User);

            var user = _userManager.Users
                .Include(app => app.PeliculaFavoritas)
                .ThenInclude(p => p.Pelicula)
                .FirstOrDefault(u => u.Id == userId);

            Pelicula pelicula = _unitOfWorkRepo.Pelicula.GetByMovieId(movieId);
            PeliculaFavorita favorita = user.PeliculaFavoritas.FirstOrDefault(pf => pf.PeliculaId == pelicula.PeliculaId);

            if (favorita == null)
            {
                user.PeliculaFavoritas.Add(new PeliculaFavorita()
                {
                    PeliculaId = pelicula.PeliculaId,
                    UserId = userId
                });
            }
            else
            {
                user.PeliculaFavoritas.Remove(favorita);
            }

            await _userManager.UpdateAsync(user);

            return Redirect(url);
        }

        [HttpPost]
        public IActionResult AddComentario(int movieId, string comentario, string url)
        {
            string userId = _userManager.GetUserId(User);
            var user = _userManager.Users
                .Include(au => au.Usuario)
                .ThenInclude(u => u.ApplicationUsers)
                .FirstOrDefault(u => u.Id == userId);

            string autor = string.Empty;
            if (string.IsNullOrEmpty(user.Usuario.Nombre) && string.IsNullOrEmpty(user.Usuario.Apellido))
            {
                autor = user.UserName.Split('@')[0];
            }
            else
            {
                autor = $"{user.Usuario.Nombre} {user.Usuario.Apellido}";
            }

                Pelicula pelicula = _unitOfWorkRepo.Pelicula.GetByMovieId(movieId);
            pelicula.Comentarios.Add(new Comentario
            {
                Autor = autor,
                Descripcion = comentario ?? "",
                FechaPublicacion = DateTime.Now
            });

            _unitOfWorkRepo.Save();
            return Redirect(url);
        }

        [HttpGet]
        public IActionResult Perfil()
        {
            string userId = _userManager.GetUserId(User);

            var user =_userManager.Users
                .Include(au => au.Usuario)
                .ThenInclude(u => u.ApplicationUsers)
                .FirstOrDefault(u => u.Id == userId);
            
            ViewData["userName"] = user.UserName;
            return View("Perfil", user.Usuario);            
        }

        [HttpPost]
        public async Task <IActionResult> Perfil(string nombre="", string apellido="", string pais="", IFormFile imagen=null)
        {
            string userId = _userManager.GetUserId(User);

            var user = _userManager.Users
                .Include(au => au.Usuario)
                .ThenInclude(u => u.ApplicationUsers)
                .FirstOrDefault(u => u.Id == userId);

            user.Usuario.Nombre = (!string.IsNullOrEmpty(nombre) && nombre != user.Usuario.Nombre) ? nombre : user.Usuario.Nombre;
            user.Usuario.Apellido = (!string.IsNullOrEmpty(apellido) && apellido != user.Usuario.Apellido) ? apellido : user.Usuario.Apellido;
            user.Usuario.Pais = (!string.IsNullOrEmpty(pais) &&  pais != user.Usuario.Pais) ? pais : user.Usuario.Pais;            
            
            if (imagen != null)
            {
                using (MemoryStream memory = new MemoryStream())
                {
                    await imagen.CopyToAsync(memory);
                    user.Usuario.Imagen = (memory.ToArray() != user.Usuario.Imagen) ? memory.ToArray() : user.Usuario.Imagen;
                }
            }
            await _userManager.UpdateAsync(user);

            return RedirectToAction("Perfil", "Home", new { userId });
        }

        public IActionResult PeliculasFav()
        {
            string userId = _userManager.GetUserId(User);

            var user = _userManager.Users
                .Include(au => au.Usuario)
                .ThenInclude(u => u.ApplicationUsers)
                .Include(app => app.PeliculaFavoritas)
                .ThenInclude(p => p.Pelicula)
                .FirstOrDefault(u => u.Id == userId);

            List<Pelicula> favoritas = new List<Pelicula>();
            foreach (PeliculaFavorita pf in user.PeliculaFavoritas)
            {
                favoritas.Add(_unitOfWorkRepo.Pelicula.GetByPelicuaId(pf.PeliculaId));
            }
            
            ViewData["usuario"] = user.Usuario;
            return View(favoritas);
        }

        public async Task<IActionResult> Bloquear()
        {
            string userId = _userManager.GetUserId(User);
            var user = _userManager.Users.FirstOrDefault(u => u.Id == userId);
            user.LockoutEnabled = true;
            DateTime today = DateTime.Now;
            DateTime answer = today.AddYears(5);
            user.LockoutEnd = answer;

            await _userManager.UpdateAsync(user);
            await _signInManager.SignOutAsync();
            return Redirect("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Votacion(int voto, int movieId, string url)
        {
            Pelicula pelicula = _unitOfWorkRepo.Pelicula.GetByMovieId(movieId);
            double totalVoto = pelicula.TotalVotos + voto;
            double media = Math.Round(totalVoto / (pelicula.PersonaVotos + 1));

            pelicula.TotalVotos = totalVoto;
            pelicula.Votos = media;
            pelicula.PersonaVotos++;

            _unitOfWorkRepo.Pelicula.Update(pelicula);
            _unitOfWorkRepo.Save();

            return Redirect(url);
        }


    }
}
