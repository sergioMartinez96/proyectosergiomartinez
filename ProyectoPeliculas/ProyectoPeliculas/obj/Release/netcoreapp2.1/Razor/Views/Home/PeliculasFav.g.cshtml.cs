#pragma checksum "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "aa54ff4d9779f2d53b688765e810a9e1092fe04c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_PeliculasFav), @"mvc.1.0.view", @"/Views/Home/PeliculasFav.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/PeliculasFav.cshtml", typeof(AspNetCore.Views_Home_PeliculasFav))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#line 3 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas;

#line default
#line hidden
#line 4 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas.Models;

#line default
#line hidden
#line 5 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Collections;

#line default
#line hidden
#line 6 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.General;

#line default
#line hidden
#line 7 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Movies;

#line default
#line hidden
#line 8 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Search;

#line default
#line hidden
#line 9 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas.ViewModel;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"aa54ff4d9779f2d53b688765e810a9e1092fe04c", @"/Views/Home/PeliculasFav.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d1f7ec5d3800ff8674bdcd3d79c4fbb5a3336a65", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_PeliculasFav : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Pelicula>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("nav-link ml-4 mt-3 text-white"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Perfil", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "PeliculasFav", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddFavorite", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(23, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
  
    Usuario usuario = ViewData["usuario"] as Usuario;
    string imageSrc = (usuario.Imagen is null) ? @"../images/user.png" : $"data:image/png;base64,{Convert.ToBase64String(usuario.Imagen)}";
    string action = ViewContext.RouteData.Values.Values.ToList()[1].ToString();
    var query = ViewContext.HttpContext.Request.QueryString.ToString();

#line default
#line hidden
            BeginContext(382, 218, true);
            WriteLiteral("\r\n<main role=\"main\">\r\n    <div class=\"row row-cols-2\">\r\n        <!-- Sidenav -->\r\n        <div class=\"col sidenav\">\r\n            <ul class=\"list-group bg-dark\">\r\n                <h4 class=\"text-white text-center mt-3\">");
            EndContext();
            BeginContext(602, 52, false);
#line 15 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
                                                    Write((usuario.Nombre != "") ? usuario.Nombre : "Username");

#line default
#line hidden
            EndContext();
            BeginContext(655, 121, true);
            WriteLiteral("</h4>\r\n                <div class=\"dropdown-divider ml-3\"></div>\r\n                <div class=\"row\">\r\n                    ");
            EndContext();
            BeginContext(776, 219, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d034f3a178304dabb89af4fa0e8cd907", async() => {
                BeginContext(859, 132, true);
                WriteLiteral("\r\n                        Perfil\r\n                        <i class=\"fa fa-user-o pr-4\" aria-hidden=\"true\"></i>\r\n                    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(995, 81, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"row\">\r\n                    ");
            EndContext();
            BeginContext(1076, 239, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f0941b5588ef4decb64d8f53caf3720a", async() => {
                BeginContext(1165, 146, true);
                WriteLiteral("\r\n                        Peliculas Favoritas\r\n                        <i class=\"fa fa-heart-o pr-4\" aria-hidden=\"true\"></i>\r\n                    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1315, 704, true);
            WriteLiteral(@"
                </div>
            </ul>
        </div>

        <!-- Contend -->
        <div class=""col contend"">
            <nav class=""navbar navbar-expand-lg bg-white"">
                <div class=""collapse navbar-collapse px-3"" id=""navbarNav"">
                    <ul class=""navbar nav"">
                        <li class=""nav-item"">
                            <h3>Peliculas Favoritas</h3>
                        </li>
                    </ul>

                    <ul class=""navbar-nav justify-content-end ml-auto"">
                        <li class=""nav-item mr-5"">
                            <button class=""btn"">
                                <img class=""rounded-circle""");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 2019, "\"", 2034, 1);
#line 45 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
WriteAttributeValue("", 2025, imageSrc, 2025, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2035, 290, true);
            WriteLiteral(@" width=""50"" height=""50"" alt="""">
                            </button>
                        </li>
                    </ul>
                </div>
            </nav>

            <!-- Favorite Movie -->
            <div class=""container"">
                <div class=""row mt-4"">
");
            EndContext();
#line 55 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
                      
                        if (Model.Count < 1)
                        {

#line default
#line hidden
            BeginContext(2422, 158, true);
            WriteLiteral("                            <div class=\"col-12\">\r\n                                <h5>No tienes peliculas favoritas</h5>\r\n                            </div>\r\n");
            EndContext();
#line 61 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
                        }
                        else
                        {
                            foreach (var pelicula in Model)
                            {
                                string img = (pelicula.ImageUrl != null) ? $"https://image.tmdb.org/t/p/w220_and_h330_face{pelicula.ImageUrl}" : "https://www.themoviedb.org/assets/2/v4/glyphicons/basic/glyphicons-basic-38-picture-grey-c2ebdbb057f2a7614185931650f8cee23fa137b93812ccb132b9df511df1cfac.svg";

#line default
#line hidden
            BeginContext(3063, 154, true);
            WriteLiteral("                                <div class=\"col-2 mb-4\">\r\n                                    <div class=\"card\">\r\n                                        ");
            EndContext();
            BeginContext(3217, 331, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "532ab2c872434880a032ed1a702716fc", async() => {
                BeginContext(3367, 174, true);
                WriteLiteral("\r\n                                            <button class=\"btn position-absolute\"><i class=\"fa fa-times text-danger\"></i></button>\r\n                                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-url", "Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 69 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
                                                                                                               WriteLiteral($"~/Home/{action}{query}");

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["url"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-url", __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["url"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 69 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
                                                                                                                                                               WriteLiteral(pelicula.MovieId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["movieId"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-movieId", __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["movieId"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3548, 46, true);
            WriteLiteral("\r\n                                        <img");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 3594, "\"", 3604, 1);
#line 72 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
WriteAttributeValue("", 3600, img, 3600, 4, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3605, 149, true);
            WriteLiteral(">\r\n\r\n                                        <div class=\"card-body\">\r\n                                            <h5 class=\"card-title text-center\">");
            EndContext();
            BeginContext(3755, 15, false);
#line 75 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
                                                                          Write(pelicula.Titulo);

#line default
#line hidden
            EndContext();
            BeginContext(3770, 139, true);
            WriteLiteral("</h5>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n");
            EndContext();
#line 79 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Home\PeliculasFav.cshtml"
                            }
                        }
                    

#line default
#line hidden
            BeginContext(3990, 91, true);
            WriteLiteral("                </div>\r\n            </div>            \r\n        </div>\r\n    </div>\r\n</main>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Pelicula>> Html { get; private set; }
    }
}
#pragma warning restore 1591
