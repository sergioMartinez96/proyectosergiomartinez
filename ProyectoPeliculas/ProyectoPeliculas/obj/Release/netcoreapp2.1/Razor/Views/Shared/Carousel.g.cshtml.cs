#pragma checksum "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "476f2922cedb64d708256805b20ef1caab63676c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Carousel), @"mvc.1.0.view", @"/Views/Shared/Carousel.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/Carousel.cshtml", typeof(AspNetCore.Views_Shared_Carousel))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#line 3 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas;

#line default
#line hidden
#line 4 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas.Models;

#line default
#line hidden
#line 5 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Collections;

#line default
#line hidden
#line 6 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.General;

#line default
#line hidden
#line 7 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Movies;

#line default
#line hidden
#line 8 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Search;

#line default
#line hidden
#line 9 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas.ViewModel;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"476f2922cedb64d708256805b20ef1caab63676c", @"/Views/Shared/Carousel.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d1f7ec5d3800ff8674bdcd3d79c4fbb5a3336a65", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Carousel : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TMDbLib.Objects.General.SearchContainer<SearchMovie>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(61, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
   
    int cnt = 0;
    int aux = 0;
    int temp = 0;
    List<int> numbers = new List<int>();
    Random random = new Random();

    for (int i = 0; i < 8; i++)
    {
        temp = random.Next(0, 20);
        if (!numbers.Contains(temp))
        {
            numbers.Add(temp);
        }
        else
        {
            i--;
        }
    }

#line default
#line hidden
            BeginContext(434, 75, true);
            WriteLiteral("\r\n<div id=\"carouselCaptions\" class=\"carousel slide\" data-ride=\"carousel\">\r\n");
            EndContext();
            BeginContext(517, 42, true);
            WriteLiteral("        <ol class=\"carousel-indicators\">\r\n");
            EndContext();
#line 27 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
              
                for (int i = 0; i < 8; i++)
                {

#line default
#line hidden
            BeginContext(639, 71, true);
            WriteLiteral("                    <li data-target=\"#carouselCaptions\" data-slide-to=\"");
            EndContext();
            BeginContext(711, 1, false);
#line 30 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
                                                                  Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(712, 1, true);
            WriteLiteral("\"");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 713, "\"", 745, 1);
#line 30 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
WriteAttributeValue("", 721, i == 0 ? "active": "", 721, 24, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(746, 8, true);
            WriteLiteral("></li>\r\n");
            EndContext();
#line 31 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
                }
            

#line default
#line hidden
            BeginContext(788, 53, true);
            WriteLiteral("        </ol>\r\n        <div class=\"carousel-inner\">\r\n");
            EndContext();
#line 35 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
              
                foreach (SearchMovie movie in Model.Results)
                {
                    if (movie.BackdropPath != null && numbers.Contains(aux))
                    {

#line default
#line hidden
            BeginContext(1039, 28, true);
            WriteLiteral("                        <div");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 1067, "\"", 1122, 3);
            WriteAttributeValue("", 1075, "carousel-item", 1075, 13, true);
            WriteAttributeValue(" ", 1088, "h-100", 1089, 6, true);
#line 40 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
WriteAttributeValue(" ", 1094, cnt == 0 ? "active" : "", 1095, 27, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1123, 35, true);
            WriteLiteral(">\r\n                            <img");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 1158, "\"", 1221, 1);
#line 41 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
WriteAttributeValue("", 1164, $"https://image.tmdb.org/t/p/w780{movie.BackdropPath}", 1164, 57, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1222, 157, true);
            WriteLiteral(" class=\"d-block w-100 h-100\" alt=\"...\" />\r\n                            <div class=\"carousel-caption d-none d-md-block\">\r\n                                <h5>");
            EndContext();
            BeginContext(1380, 11, false);
#line 43 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
                               Write(movie.Title);

#line default
#line hidden
            EndContext();
            BeginContext(1391, 174, true);
            WriteLiteral("</h5>\r\n                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>\r\n                            </div>\r\n                        </div>\r\n");
            EndContext();
#line 47 "C:\Users\Sergio\Desktop\Prueba\ProyectoPeliculas\ProyectoPeliculas\Views\Shared\Carousel.cshtml"
                        cnt++;
                    }
                    else if (movie.BackdropPath == null)
                    {
                        for (int i = 0; i < 20; i++)
                        {
                            temp = random.Next(aux, 20);
                            if (!numbers.Contains(temp))
                            {
                                numbers.Remove(aux);
                                numbers.Add(temp);
                                break;
                            }
                        }

                    }
                    aux++;
                    if (cnt == 9) { break; }
                }
            

#line default
#line hidden
            BeginContext(2266, 16, true);
            WriteLiteral("        </div>\r\n");
            EndContext();
            BeginContext(2289, 466, true);
            WriteLiteral(@"
    <a class=""carousel-control-prev"" href=""#carouselCaptions"" role=""button"" data-slide=""prev"">
        <span class=""carousel-control-prev-icon"" aria-hidden=""true""></span>
        <span class=""sr-only"">Previous</span>
    </a>
    <a class=""carousel-control-next"" href=""#carouselCaptions"" role=""button"" data-slide=""next"">
        <span class=""carousel-control-next-icon"" aria-hidden=""true""></span>
        <span class=""sr-only"">Next</span>
    </a>
</div>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TMDbLib.Objects.General.SearchContainer<SearchMovie>> Html { get; private set; }
    }
}
#pragma warning restore 1591
