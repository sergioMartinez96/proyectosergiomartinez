#pragma checksum "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "76f0fc89896175d36d7f86f55f41675855de5b06"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_ListadoCines), @"mvc.1.0.view", @"/Views/Admin/ListadoCines.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Admin/ListadoCines.cshtml", typeof(AspNetCore.Views_Admin_ListadoCines))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#line 3 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas;

#line default
#line hidden
#line 4 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas.Models;

#line default
#line hidden
#line 5 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Collections;

#line default
#line hidden
#line 6 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.General;

#line default
#line hidden
#line 7 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Movies;

#line default
#line hidden
#line 8 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using TMDbLib.Objects.Search;

#line default
#line hidden
#line 9 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\_ViewImports.cshtml"
using ProyectoPeliculas.ViewModel;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"76f0fc89896175d36d7f86f55f41675855de5b06", @"/Views/Admin/ListadoCines.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d1f7ec5d3800ff8674bdcd3d79c4fbb5a3336a65", @"/Views/_ViewImports.cshtml")]
    public class Views_Admin_ListadoCines : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Cine>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "ListadoCines", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "InsertarCine", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Admin", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-success"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("color:white; float:right"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "EliminarCine", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "EditarCine", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(20, 336, true);
            WriteLiteral(@"
    <div class=""container-fluid mt-3"">
        <div class=""row"">
            <div class=""barra-lateral col-12 col-sm-auto"">
                <div class=""logo"">
                    <h2>Dashboard</h2>
                </div>
                <nav class=""menu d-flex d-sm-block justify-content-center flex-wrap"">
                    ");
            EndContext();
            BeginContext(356, 79, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d8c7ca684f8d4d91ae98ce2bd88c962d", async() => {
                BeginContext(378, 53, true);
                WriteLiteral("<i class=\"fa fa-users mr-2\"></i><span>Usuarios</span>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(435, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(457, 84, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3a8469b9ab7149049db28c9b72a2a028", async() => {
                BeginContext(486, 51, true);
                WriteLiteral("<i class=\"fa fa-film mr-2\"></i> <span>Cines</span> ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(541, 311, true);
            WriteLiteral(@"
                </nav>
            </div>

            <main class=""main col"">
                <div class=""row"">
                    <div class=""columna col-lg-10"">
                        <h4>Lista de cines</h4>
                        <table class=""table table-striped"">
                            ");
            EndContext();
            BeginContext(852, 183, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "bf8d4d93562d4838acc1de1e2a0413fe", async() => {
                BeginContext(961, 70, true);
                WriteLiteral("\r\n                                Nuevo<i class=\"fa fa-plus ml-2\"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1035, 437, true);
            WriteLiteral(@"
                                <thead class=""mt-2"">
                                    <tr>
                                        <th class=""text-center col-2"">Nombre</th>
                                        <th class=""text-center col-3"">Direccion</th>
                                        <th class=""text-center col-2"">Ciudad</th>
                                        <th class=""text-center col-3"">Pagina web</th>
");
            EndContext();
            BeginContext(1640, 128, true);
            WriteLiteral("                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n\r\n");
            EndContext();
#line 33 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                     foreach (var cine in Model)
                                    {

#line default
#line hidden
            BeginContext(1873, 120, true);
            WriteLiteral("                                        <tr>\r\n                                            <td class=\"text-center col-2\">");
            EndContext();
            BeginContext(1994, 11, false);
#line 36 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                                                     Write(cine.Nombre);

#line default
#line hidden
            EndContext();
            BeginContext(2005, 81, true);
            WriteLiteral("</td>\r\n                                            <td class=\"text-center col-3\">");
            EndContext();
            BeginContext(2087, 14, false);
#line 37 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                                                     Write(cine.Direccion);

#line default
#line hidden
            EndContext();
            BeginContext(2101, 81, true);
            WriteLiteral("</td>\r\n                                            <td class=\"text-center col-2\">");
            EndContext();
            BeginContext(2183, 11, false);
#line 38 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                                                     Write(cine.Ciudad);

#line default
#line hidden
            EndContext();
            BeginContext(2194, 81, true);
            WriteLiteral("</td>\r\n                                            <td class=\"text-center col-3\">");
            EndContext();
            BeginContext(2276, 14, false);
#line 39 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                                                     Write(cine.PaginaWeb);

#line default
#line hidden
            EndContext();
            BeginContext(2290, 133, true);
            WriteLiteral("</td>\r\n                                            <td class=\"text-center col-2\">\r\n\r\n                                                ");
            EndContext();
            BeginContext(2423, 113, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "041f63517a36460d93ca26ee897a9037", async() => {
                BeginContext(2503, 29, true);
                WriteLiteral("<i class=\"fa fa-trash-o\"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_7.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-idCine", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 42 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                                                                                          WriteLiteral(cine.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["idCine"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-idCine", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["idCine"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2536, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(2651, 48, true);
            WriteLiteral("                                                ");
            EndContext();
            BeginContext(2699, 132, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c6ed8b7e20444712a7fd5de8daa344db", async() => {
                BeginContext(2801, 26, true);
                WriteLiteral("<i class=\"fa fa-edit\"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_9.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_9);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-idCine", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 44 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                                                                                         WriteLiteral(cine.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["idCine"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-idCine", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["idCine"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2831, 100, true);
            WriteLiteral("\r\n                                            </td>\r\n                                        </tr>\r\n");
            EndContext();
#line 47 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                    }

#line default
#line hidden
            BeginContext(2970, 437, true);
            WriteLiteral(@"
                                </tbody>



</table>

                    </div>

                    <div class=""columna col-lg-2"">
                        <div class=""widget estadisticas"">
                            <h3 class=""text-center"">Estadísticas</h3>
                            <div class=""contenedor d-flex flex-wrap"">
                                <div class=""caja"">
                                    <h3>");
            EndContext();
            BeginContext(3408, 11, false);
#line 62 "C:\Users\Sergio\Desktop\ProyectoDaw2SergioMartinez\proyectosergiomartinez\ProyectoPeliculas\ProyectoPeliculas\Views\Admin\ListadoCines.cshtml"
                                   Write(Model.Count);

#line default
#line hidden
            EndContext();
            BeginContext(3419, 270, true);
            WriteLiteral(@"</h3>
                                    <p>Cines</p>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </main>
        </div>
    </div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Cine>> Html { get; private set; }
    }
}
#pragma warning restore 1591
