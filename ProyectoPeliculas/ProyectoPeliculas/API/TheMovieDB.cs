﻿using ProyectoPeliculas.Models;
using System;
using System.Threading.Tasks;
using TMDbLib.Client;
using TMDbLib.Objects.Collections;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;

namespace ProyectoPeliculas.API
{
    // Clase estatica para la consulta de informacion al API The Movie DataBase API 3

    public static class TheMovieDB
    {
        // Creacion del cliente para realizar las consultas al API
        private static TMDbClient Client = new TMDbClient("bf7a0d7e84fbc649f8d6f2819491a0d6");

        //Current Query
        public static string Query { get; set; }

        // Current Genre
        public static int GenreId { get; set; } = 0;
        public static int CurrentPage { get; set; } = 5;

        #region Get List Movie by 
        // Obtener las peliculas Populares
        public static async Task<SearchContainer<SearchMovie>> GetPopularMovies(int page = 1)
        {
            SearchContainer<SearchMovie> popularMovies = await Client.GetMoviePopularListAsync(language:"es-ES",page:page);
            return popularMovies;
        }

        // Obtener las peliculas Vistas ahora?
        public static async Task<SearchContainer<SearchMovie>> GetNowPlayingMovies(int page = 1)
        {
            SearchContainer<SearchMovie> nowPlayingMovies = await Client.GetMovieNowPlayingListAsync(language: "es-ES",page:page);
            return nowPlayingMovies;
        }

        // Obtener las peliculas Proximas a estrenar?
        public static async Task<SearchContainer<SearchMovie>> GetUpComingMovies(int page = 1)
        {
            SearchContainer<SearchMovie> upComingMovie = await Client.GetMovieUpcomingListAsync(language: "es-ES",page:page);
            return upComingMovie;
        }

        // Obtener las pelicuas mejor calificadas
        public static async Task<SearchContainer<SearchMovie>> GetTopRateMovies(int page = 1)
        {
            SearchContainer<SearchMovie> topRateMovies = await Client.GetMovieTopRatedListAsync(language: "es-ES",page:page);
            return topRateMovies;
        }

        //Obtener la pelicuas mas recientes (valor devuelto por ella es variable)
        public static async Task<Movie> GetLatestMovies()
        {
            Movie lastestMovies = await Client.GetMovieLatestAsync();
            return lastestMovies;
        }
        #endregion

        #region Collections Movies
        // Obtener la collecion de peliculas
        public static async Task<Collection> GetCollectionMovies(int collectionId)
        {
            Collection collectionMovies = await Client.GetCollectionAsync(collectionId);
            return collectionMovies;
        }
        #endregion

        #region Get Movie
        // sobrecarga del metodo GetMovie lang = Lenguaje (codificacion usada ISO 639-1) (parametro opcional)
        public static async Task<Movie> GetMovie(int movieId, string lang = "es-ES")
        {
            Movie movie = await Client.GetMovieAsync(movieId, language: lang);
            return movie;
        }
        #endregion

        #region Search Movies
        // Buscar una pelicula por medio de una query = <nombre>, <titulo>, <letra inicial>
        public static async Task<SearchContainer<SearchMovie>> SearchMovie(int page, string lang = "es-ES")
        {
            SearchContainer<SearchMovie> searchMovie = await Client.SearchMovieAsync(Query, language: lang, page: page);
            return searchMovie;
        }

        public static async Task<SearchContainer<SearchMovie>> SearchByGenre(int page = 1)
        {
            SearchContainer<SearchMovie> searchMovies = await Client.GetGenreMoviesAsync(genreId: GenreId, page: page);
            CurrentPage = (searchMovies.Page >= CurrentPage + 1) ? CurrentPage + 5 : CurrentPage;
            CurrentPage = (searchMovies.Page > CurrentPage) ? searchMovies.Page : CurrentPage;
            return searchMovies;
        }
        #endregion


    }
}
