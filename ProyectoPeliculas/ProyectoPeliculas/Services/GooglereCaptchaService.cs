﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProyectoPeliculas.Models
{
    public class GooglereCaptchaService
    {
        private ReCAPTCHASettings _settings;

         public GooglereCaptchaService(IOptions<ReCAPTCHASettings> settings)
        {
            _settings = settings.Value;
        }


        public virtual async Task<GoogleResponse> RecVer(string _Token)
        {
            GooglereCaptchaData _Mydata = new GooglereCaptchaData
            {
                Response = _Token,
                Secret = _settings.ReCAPTCHA_Secret_Key
            };

            HttpClient client = new HttpClient();
            var respponse = await client.GetStringAsync($"https://www.google.com/recaptcha/api/siteverify?secret={_Mydata.Secret}&response={_Mydata.Response}");
            var capresp = JsonConvert.DeserializeObject<GoogleResponse>(respponse);

            return capresp;
        }
    }

    public class GooglereCaptchaData
    {
        public string Response { get; set; } // Token
        public string Secret { get; set; } 
    }

    public class GoogleResponse
    {
        public bool Sucess { get; set; }
        public double Score{ get; set; }
        public string Action { get; set; }
        public DateTime Challenge_ts { get; set; }
        public string Hostname { get; set; }
    }

}
