﻿using System;
using System.Collections.Generic;
using ProyectoPeliculas.Models;

namespace ProyectoPeliculas.Interfaces
{
    public interface ICine
    {
        List<Cine> GetAll();

        Cine Get(int cineId);

        Cine GetById(int cineId);

        void Insert(Cine cine);

        void Update(Cine cine);

        void Delete(Cine cine);
    }
}
