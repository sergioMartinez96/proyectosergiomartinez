﻿using System;
using System.Collections.Generic;
using ProyectoPeliculas.Models;

namespace ProyectoPeliculas.Interfaces
{
    public interface IUsuario
    {
        Usuario Get(string userId);

        void Insert(Usuario usuario);

        void Update(Usuario usuario);
    }
}
