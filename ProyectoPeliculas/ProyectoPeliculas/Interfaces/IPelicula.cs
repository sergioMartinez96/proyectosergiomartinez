﻿using System;
using System.Collections.Generic;
using ProyectoPeliculas.Models;

namespace ProyectoPeliculas.Interfaces
{
    public interface IPelicula
    {
        List<Pelicula> GetAll();

        Pelicula GetByMovieId(int peliculaId);

        Pelicula GetByPelicuaId(int peliuclaId);

        void Insert(Pelicula pelicula);

        void Update(Pelicula pelicula);

        void Delete(Pelicula pelicula);
    }
}
