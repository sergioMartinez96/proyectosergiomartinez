﻿using System;
using System.Collections.Generic;
using ProyectoPeliculas.Models;

namespace ProyectoPeliculas.Interfaces
{
    public interface ICategoria
    {
        List<Categoria> GetAll();

        Categoria Get(int categoriaId);

        Categoria GetById(int categoriaId);

        void Insert(Categoria categoria);

        void Update(Categoria categoria);

        void Delete(Categoria categoria);
    }
}
