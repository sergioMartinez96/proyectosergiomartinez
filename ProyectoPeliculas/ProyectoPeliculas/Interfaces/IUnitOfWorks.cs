﻿using System;

namespace ProyectoPeliculas.Interfaces
{
    public interface IUnitOfWorks
    {
        IPelicula Pelicula { get; }

        ICategoria Categoria { get; }

        ICine Cine { get; }
        void Save();
    }
}
