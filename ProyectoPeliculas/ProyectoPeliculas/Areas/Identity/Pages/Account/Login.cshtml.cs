﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ProyectoPeliculas.Models;

namespace ProyectoPeliculas.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<LoginModel> _logger;
        private readonly GooglereCaptchaService _GooglereCaptchaService;

        public LoginModel(SignInManager<ApplicationUser> signInManager, ILogger<LoginModel> logger, GooglereCaptchaService googlereCaptchaService)
        {
            _signInManager = signInManager;
            _logger = logger;
            _GooglereCaptchaService = googlereCaptchaService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "El email es obligatorio")]
            [EmailAddress]
            public string Email { get; set; }

            [Required(ErrorMessage = "La contraseña es obligatoria")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "¿Recordar?")]
            public bool RememberMe { get; set; }

            [Required]
            public string Token { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null, bool showModal = false)
        {
            ViewData["showModal"] = showModal;

            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            // Google Recaptcha
            var _GooglereCaptcha = _GooglereCaptchaService.RecVer(Input.Token);

            if (!_GooglereCaptcha.Result.Sucess && _GooglereCaptcha.Result.Score <= 0.5)
            {
                ModelState.AddModelError(string.Empty, "No eres un humano ...");
                return Page();
            }


            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, Input.RememberMe, lockoutOnFailure: true);
                if (result.Succeeded && !result.IsLockedOut)
                {
                    ViewData["showModal"] = false;
                    _logger.LogInformation("User logged in.");
                    return LocalRedirect(returnUrl);

                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    return RedirectToPage("./Login", new { returnUrl, showModal = true });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Login no válido.");
                    ViewData["showModal"] = false;
                    return Page();
                }

            }

            // If we got this far, something failed, redisplay form
            ViewData["showModal"] = false;
            return Page();
        }
    }
}
